/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game;

import com.badlogic.gdx.Game;
import com.starfighter.game.screens.MainScreen;

public class StarfighterGame extends Game {

    private State gameState;

    @Override
    public void create() {
        Assets.load();
        Assets.music.play();
        Assets.music.setVolume(Params.MUSIC_VOLUME);
        Assets.music.setLooping(true);

        gameState = StarfighterGame.State.RUNNING;

        setScreen(new MainScreen(this));
    }

    @Override
    public void dispose() {
        super.dispose();
        Assets.music.stop();
        Assets.dispose();
        getScreen().dispose();
    }

    @Override
    public void pause() {
        super.pause();

        getScreen().pause();
    }

    public void togglePause() {
        if (gameState == State.PAUSE) {
            gameState = State.RUNNING;
        } else {
            gameState = State.PAUSE;
        }
    }

    public State getGameState() {
        return gameState;
    }

    public void setGameStatePause() {
        this.gameState = State.PAUSE;
    }

    public enum State {RUNNING, PAUSE}
}
