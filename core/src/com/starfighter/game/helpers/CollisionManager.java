/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.utils.Array;
import com.starfighter.game.GameWorld;
import com.starfighter.game.Status;
import com.starfighter.game.gameobjects.GameObject;

public class CollisionManager {

    private Array<GameObject> allyGameObjects;
    private Array<GameObject> enemyGameObjects;
    private GameWorld gameWorld;

    public CollisionManager(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        allyGameObjects = new Array<GameObject>(false, 25);
        enemyGameObjects = new Array<GameObject>(false, 125);
    }

    public void handleCollisions() {
        allocateGameObjects();

        for (GameObject ally : allyGameObjects) {
            for (GameObject enemy : enemyGameObjects) {
                collide(ally, enemy);
            }
        }
    }

    private void allocateGameObjects() {
        allyGameObjects.clear();
        enemyGameObjects.clear();

        for (GameObject object : gameWorld.getGameObjects()) {
            if (object.isAlive()) {
                if (object.getStatus() == Status.ALLY) {
                    allyGameObjects.add(object);
                } else {
                    enemyGameObjects.add(object);
                }
            }
        }
    }

    private void collide(GameObject ally, GameObject enemy) {
        if (!(ally.getHp() == 100 && enemy.getHp() == 100) && ally.hit(enemy.getBounds())) {
            if (ally.getHp() == 100) {
                enemy.setHp(enemy.getHp() - 1);
            } else if (enemy.getHp() == 100) {
                ally.setHp(ally.getHp() - 1);
            } else {
                enemy.setHp(enemy.getHp() - 1);
                ally.setHp(ally.getHp() - 1);
            }

            if (ally.getHp() <= 0) {
                showNameInput();

                ally.explode(gameWorld.getExplosionEffectPool(), gameWorld.getExplosionEffects());
            }

            if (enemy.getHp() == 100) {
                enemy.setDead();
            }

            if (ally.getHp() == 100) {
                ally.setDead();
            }

            if (enemy.getHp() <= 0) {
                enemy.explode(gameWorld.getExplosionEffectPool(), gameWorld.getExplosionEffects());
                gameWorld.setScore(gameWorld.getScore() + enemy.getValue());
            }
        }

    }

    private void showNameInput() {
        Gdx.input.getTextInput(new Input.TextInputListener() {
            @Override
            public void input(String text) {
                gameWorld.setName(text);
            }

            @Override
            public void canceled() {
                gameWorld.setName("unknown");
            }
        }, "Name", "", "Name");
    }
}
