/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.starfighter.game.Params;
import com.starfighter.game.Score;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ScoreManager {

    private List<Score> scores;
    private Preferences highScores;
    private int lastScoreRank;

    public ScoreManager() {
        highScores = Gdx.app.getPreferences(Params.HIGH_SCORES_PREF);
        scores = new ArrayList<Score>();
        readScores();
        lastScoreRank = -1;
    }

    private void readScores() {
        @SuppressWarnings(value = "unchecked")
        Map<String, String> map = (Map<String, String>) highScores.get();

        for (Map.Entry<String, ?> entry : map.entrySet()) {
            String key = entry.getKey();
            Object object = entry.getValue();
            int value;
            if (object instanceof String) {
                value = Integer.valueOf((String) object);
            } else {
                value = (Integer) object;
            }

            scores.add(new Score(key, value));
        }

        Collections.sort(scores, Collections.reverseOrder());
    }

    public void writeScore(String name, int result) {
        Score score = new Score(name, result);
        scores.add(score);
        Collections.sort(scores, Collections.reverseOrder());
        lastScoreRank = scores.indexOf(score);

        for (int i = 0; i < 10; i++) {
            if (scores.size() > i) {
                String key = scores.get(i).getName();
                int value = scores.get(i).getValue();
                highScores.putInteger(key, value);
            }
        }
        highScores.flush();
    }

    public List<Score> getScores() {
        return scores;
    }

    public int getLastScoreRank() {
        return lastScoreRank;
    }
}
