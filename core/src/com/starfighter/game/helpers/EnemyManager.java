/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.helpers;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;
import com.starfighter.game.GameWorld;
import com.starfighter.game.Params;
import com.starfighter.game.gameobjects.GameObject;
import com.starfighter.game.gameobjects.enemies.AdvancedEnemy;
import com.starfighter.game.gameobjects.enemies.BasicEnemy;
import com.starfighter.game.gameobjects.enemies.Boss;
import com.starfighter.game.gameobjects.enemies.Enemy;

public class EnemyManager {

    private GameWorld gameWorld;
    private Boss boss;
    private int enemiesCount;
    private Array<GameObject> gameObjects;
    private float timeSinceDifficultyUpdate;
    private float bossTimer;
    private float difficulty;

    public EnemyManager(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        gameObjects = gameWorld.getGameObjects();

        difficulty = 1.0f;
    }

    public void spawnEnemies(float delta) {
        timeSinceDifficultyUpdate += delta;
        bossTimer += delta;

        if (timeSinceDifficultyUpdate >= Params.DIFFICULTY_UPDATE_DELAY) {
            timeSinceDifficultyUpdate = 0;
            difficulty /= Params.DIFFICULTY_COEFFICIENT_X;
        }

        if (boss == null || boss.getHp() <= 0) {
            if (bossTimer >= Params.BOSS_SPAWN_TIME) {
                spawnBoss();
            }

            if (enemiesCount < Params.MAX_ENEMIES) {
                if (MathUtils.random(Params.BASIC_ENEMY_SPAWN_RATE) == 0) {
                    spawnBasicEnemy(difficulty);
                }
                if (MathUtils.random(Params.ADVANCED_ENEMY_SPAWN_RATE) == 0) {
                    spawnAdvancedEnemy(difficulty);
                }
            }
        } else {
            bossTimer = 0;
        }
    }

    private void spawnBoss() {
        bossTimer = 0;

        boss = Pools.obtain(Boss.class);
        boss.init(gameWorld.getScreenWidth() + 50, gameWorld.getScreenHeight() / 2 - 64,
                gameWorld);
        boss.setDelay(Params.BOSS_FIRE_DELAY * difficulty + Params.DIFFICULTY_COEFFICIENT_Y);

        gameObjects.add(boss);
    }

    private void spawnBasicEnemy(float difficulty) {
        float startY = MathUtils.random(0, gameWorld.getScreenHeight() - 48);

        Enemy enemy = Pools.obtain(BasicEnemy.class);
        enemy.init(gameWorld.getScreenWidth() + 50, startY, gameWorld);
        enemy.setDelay(Params.BASIC_ENEMY_FIRE_DELAY * difficulty + Params.DIFFICULTY_COEFFICIENT_Y);
        gameObjects.add(enemy);

        enemiesCount++;
    }

    private void spawnAdvancedEnemy(float difficulty) {
        Enemy enemy = Pools.obtain(AdvancedEnemy.class);
        enemy.init(gameWorld.getScreenWidth() + 50, gameWorld.getScreenHeight() / 2 - 32, gameWorld);
        enemy.setDelay(Params.ADVANCED_ENEMY_FIRE_DELAY * difficulty + Params.DIFFICULTY_COEFFICIENT_Y);
        gameObjects.add(enemy);

        enemiesCount++;
    }

    public void decrementEnemiesCount() {
        if(enemiesCount > 0) {
            enemiesCount--;
        }
    }

}
