/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.helpers;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.starfighter.game.Button;
import com.starfighter.game.GameWorld;
import com.starfighter.game.gameobjects.Starfighter;

public class InputHandler implements InputProcessor {

    private Starfighter starfighter;
    private Button sButton;
    private Button pauseButton;
    private float scaleFactorX;
    private float scaleFactorY;
    private GameWorld gameWorld;

    public InputHandler(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        this.starfighter = gameWorld.getStarfighter();
        this.scaleFactorX = com.starfighter.game.Params.SCALE_FACTOR_X;
        this.scaleFactorY = com.starfighter.game.Params.SCALE_FACTOR_Y;

        sButton = gameWorld.getShoot();
        pauseButton = gameWorld.getPause();
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Keys.W:
                starfighter.moveUp(true);
                break;

            case Keys.S:
                starfighter.moveDown(true);
                break;

            case Keys.SPACE:
                starfighter.setShooting(true);
                break;
        }

        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Keys.W:
                starfighter.moveUp(false);
                break;

            case Keys.S:
                starfighter.moveDown(false);
                break;

            case Keys.SPACE:
                starfighter.setShooting(false);
                break;
        }

        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        screenX = scaleX(screenX);
        screenY = scaleY(screenY);

        int height = (int) starfighter.getGameWorld().getScreenHeight();
        int width = (int) starfighter.getGameWorld().getScreenWidth();

        if (sButton.contains(screenX, screenY)) {
            starfighter.setShooting(true);

        } else if (pauseButton.contains(screenX, screenY)) {
            gameWorld.getGame().togglePause();
        } else if (screenX < width * 0.625) {
            if (screenY <= height / 2) {
                starfighter.moveUp(true);
            } else if (screenY > height / 2) {
                starfighter.moveDown(true);
            }
        }

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        screenX = scaleX(screenX);
        screenY = scaleY(screenY);
        if (!sButton.contains(screenX, screenY)) {
            starfighter.moveUp(false);
            starfighter.moveDown(false);
        } else {
            starfighter.setShooting(false);
        }

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        // TODO Auto-generated method stub
        return false;
    }

    private int scaleX(int screenX) {
        return (int) (screenX * scaleFactorX);
    }

    private int scaleY(int screenY) {
        return (int) (screenY * scaleFactorY);
    }
}
