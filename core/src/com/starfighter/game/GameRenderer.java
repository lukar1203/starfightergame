/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.starfighter.game.gameobjects.GameObject;

public class GameRenderer {

    public static final String SCORE = "SCORE: ";
    public static final String FPS = "FPS: ";
    private GameWorld gameWorld;
    private OrthographicCamera camera;

    private SpriteBatch spriteBatch;

    private Array<GameObject> objects;
    private Array<PooledEffect> explosionEffects;

    private Background background1;
    private Background background2;

    private Button sButton;
    private StringBuilder scoreString;
    private Button pauseButton;
    private float upperUiPointY;

    public GameRenderer(GameWorld gameWorld, OrthographicCamera camera) {
        this.gameWorld = gameWorld;
        this.camera = camera;

        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(camera.combined);

        objects = gameWorld.getGameObjects();
        explosionEffects = gameWorld.getExplosionEffects();

        BackgroundScroller backgroundScroller = gameWorld.getBackgroundScroller();
        background1 = backgroundScroller.getBackground1();
        background2 = backgroundScroller.getBackground2();
        sButton = gameWorld.getShoot();
        pauseButton = gameWorld.getPause();
        upperUiPointY = pauseButton.getY() + pauseButton.getHeight() / 2.0f;

        scoreString = new StringBuilder();
    }

    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        spriteBatch.setProjectionMatrix(camera.combined);

        spriteBatch.begin();
        spriteBatch.disableBlending();
        background1.draw(spriteBatch);
        background2.draw(spriteBatch);
        spriteBatch.enableBlending();

        drawObjects();

        for (PooledEffect effect : explosionEffects) {
            effect.draw(spriteBatch);
        }

        sButton.draw(spriteBatch);
        pauseButton.draw(spriteBatch);
        drawFpsCounter();
        drawScore();
        drawHpBar();

        spriteBatch.end();
    }

    private void drawObjects() {
        for (GameObject object : objects) {
            object.draw(spriteBatch);
        }
    }

    private void drawFpsCounter() {
        if (Params.DEBUG) {
            Assets.font.draw(spriteBatch, FPS + Integer.toString(Gdx.graphics.getFramesPerSecond()),
                    gameWorld.getScreenWidth() / 2 - 10, 10);
        }
    }

    private void drawScore() {
        scoreString.setLength(0);
        scoreString = scoreString.append(SCORE).append(gameWorld.getScore());

        float length = Assets.font.getBounds(scoreString).width;
        Assets.font.draw(spriteBatch, scoreString, gameWorld.getScreenWidth() - length * 1.1f -
                        (gameWorld.getScreenWidth() - pauseButton.getX()),
                upperUiPointY - Assets.font.getLineHeight() / 2.0f);
    }

    private void drawHpBar() {
        float width = ((float) gameWorld.getStarfighter().getHp() / (float) Params.STARFIGHTER_MAX_HP) *
                150.0f;
        int hpBarHeight = Assets.bar.getRegionHeight();
        Assets.bar.setSize(width, hpBarHeight);
        Assets.bar.setPosition(10, upperUiPointY - hpBarHeight / 2);
        Assets.bar.draw(spriteBatch);
    }

    public void dispose() {
        spriteBatch.dispose();
    }
}
