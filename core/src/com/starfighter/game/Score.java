/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game;

public class Score implements Comparable<Score> {

    private String name;
    private int value;

    public Score(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public int compareTo(Score score) {
        if (value > score.value) {
            return 1;
        }
        if (value < score.value) {
            return -1;
        }

        return 0;
    }

    @Override
    public String toString() {
        return name + " - " + value;
    }
}
