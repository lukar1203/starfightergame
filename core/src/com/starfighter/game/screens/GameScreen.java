/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.screens;

import com.badlogic.gdx.Gdx;
import com.starfighter.game.GameRenderer;
import com.starfighter.game.GameWorld;
import com.starfighter.game.StarfighterGame;
import com.starfighter.game.helpers.InputHandler;

public class GameScreen extends AbstractScreen {

    private GameWorld world;
    private GameRenderer renderer;

    public GameScreen(StarfighterGame game) {
        super(game);
        world = new GameWorld(game, backgroundScroller);
        renderer = new GameRenderer(world, camera);

        Gdx.input.setInputProcessor(new InputHandler(world
        ));
    }

    @Override
    public void render(float delta) {
        world.update(delta);
        renderer.render();
    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void pause() {
        game.setGameStatePause();

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        renderer.dispose();
    }

}
