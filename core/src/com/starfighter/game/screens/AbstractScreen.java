/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.starfighter.game.Background;
import com.starfighter.game.BackgroundScroller;
import com.starfighter.game.Params;
import com.starfighter.game.StarfighterGame;

public abstract class AbstractScreen implements Screen {

    protected StarfighterGame game;
    protected BackgroundScroller backgroundScroller;
    protected Background background1;
    protected Background background2;
    protected SpriteBatch spriteBatch;
    protected OrthographicCamera camera;

    protected AbstractScreen(StarfighterGame game) {
        this.game = game;
        backgroundScroller = new BackgroundScroller();
        background1 = backgroundScroller.getBackground1();
        background2 = backgroundScroller.getBackground2();
        spriteBatch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(true, Params.SCREEN_WIDTH, Params.SCREEN_HEIGHT);

        spriteBatch.setProjectionMatrix(camera.combined);
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
    }
}
