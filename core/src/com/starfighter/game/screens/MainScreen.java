/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector3;
import com.starfighter.game.Assets;
import com.starfighter.game.Button;
import com.starfighter.game.Params;
import com.starfighter.game.StarfighterGame;

public class MainScreen extends AbstractScreen {

    private Sprite sprite;
    private Button button;

    public MainScreen(StarfighterGame game) {
        super(game);

        button = new Button(Params.SCREEN_WIDTH - Assets.highScoresButton.getRegionWidth() * 1.1f,
                Params.SCREEN_HEIGHT - Assets.highScoresButton.getRegionHeight() * 1.5f,
                Assets.highScoresButton, 0);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.begin();
        spriteBatch.disableBlending();
        background1.draw(spriteBatch);
        background2.draw(spriteBatch);
        spriteBatch.enableBlending();
        backgroundScroller.update(delta);
        sprite.draw(spriteBatch);
        button.draw(spriteBatch);
        spriteBatch.end();

        if (Gdx.input.justTouched() || Gdx.input.isKeyPressed(Input.Keys.ANY_KEY)) {
            changeScreen();
        }
    }

    private void changeScreen() {
        Vector3 touchPoint = new Vector3();
        Vector3 unprojectedTouchPoint = camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
        if (button.getBounds().contains(unprojectedTouchPoint.x, unprojectedTouchPoint.y)) {
            dispose();
            game.setScreen(new ScoreScreen(game));
        } else {
            dispose();
            game.setScreen(new GameScreen(game));
        }
    }

    @Override
    public void show() {
        super.show();

        sprite = new Sprite(Assets.mainScreen);
        sprite.setSize(sprite.getWidth(), sprite.getHeight());
        sprite.setPosition((Params.SCREEN_WIDTH / 2) - (sprite.getWidth() / 2),
                (Params.SCREEN_HEIGHT / 2) - (sprite.getHeight() / 2));

    }
}
