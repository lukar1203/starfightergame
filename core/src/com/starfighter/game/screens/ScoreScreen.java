/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector3;
import com.starfighter.game.Assets;
import com.starfighter.game.Button;
import com.starfighter.game.Params;
import com.starfighter.game.Score;
import com.starfighter.game.StarfighterGame;
import com.starfighter.game.helpers.ScoreManager;

public class ScoreScreen extends AbstractScreen {

    public static final String HIGH_SCORES = "H I G H   S C O R E S";
    private Button button;
    private ScoreManager scoreManager;

    public ScoreScreen(StarfighterGame game) {
        super(game);
        scoreManager = new ScoreManager();

        button = new Button(Params.SCREEN_WIDTH / 2 - Assets.playAgainButton.getRegionWidth() / 2,
                Params.SCREEN_HEIGHT / 1.5f + 50, Assets.playAgainButton, 0);
    }

    public ScoreScreen(StarfighterGame game, ScoreManager scoreManager) {
        super(game);
        this.scoreManager = scoreManager;

        button = new Button(Params.SCREEN_WIDTH / 2 - Assets.playAgainButton.getRegionWidth() / 2,
                Params.SCREEN_HEIGHT / 1.5f + 50, Assets.playAgainButton, 0);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.begin();
        spriteBatch.disableBlending();
        background1.draw(spriteBatch);
        background2.draw(spriteBatch);
        spriteBatch.enableBlending();
        backgroundScroller.update(delta);
        drawScores();

        button.draw(spriteBatch);
        spriteBatch.end();

        if (Gdx.input.justTouched() || Gdx.input.isKeyPressed(Input.Keys.ANY_KEY)) {
            changeToMainScreen();
        }
    }

    private void drawScores() {
        float startHeight = Params.SCREEN_HEIGHT * 0.3f;
        float fontHeight = Assets.font.getLineHeight() * 1.1f;
        Assets.font.setScale(2.0f);
        float length = Assets.font.getBounds(HIGH_SCORES).width;
        float x = Params.SCREEN_WIDTH / 2.0f - length / 2.0f;
        Assets.font.setColor(Color.ORANGE);
        Assets.font.draw(spriteBatch, HIGH_SCORES, x, startHeight - fontHeight * 2);
        Assets.font.setColor(Color.WHITE);
        Assets.font.setScale(1.0f);

        for (int i = 0; i < 10; i++) {
            if (scoreManager.getScores().size() > i) {
                drawRow(startHeight, fontHeight, i);
            }
        }
    }

    private void changeToMainScreen() {
        Vector3 touchPoint = new Vector3();
        Vector3 unprojectedTouchPoint = camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
        if (button.getBounds().contains(unprojectedTouchPoint.x, unprojectedTouchPoint.y)) {
            dispose();
            game.setScreen(new MainScreen(game));
        }
    }

    private void drawRow(float startHeight, float fontHeight, int i) {
        float length;
        float x;
        Score score = scoreManager.getScores().get(i);

        if (scoreManager.getLastScoreRank() == i) {
            Assets.font.setColor(Color.GREEN);
        }
        String highScoresRow = (i + 1) + ". " + score;
        length = Assets.font.getBounds(highScoresRow).width;
        x = Params.SCREEN_WIDTH / 2.0f - length / 2.0f;
        Assets.font.draw(spriteBatch, (i + 1) + ". " + score, x, startHeight + fontHeight * (i + 1));

        if (scoreManager.getLastScoreRank() == i) {
            Assets.font.setColor(Color.WHITE);
        }
    }

}
