/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game;

public class BackgroundScroller {

    public static final float SPEED = -60;
    private Background background1;
    private Background background2;

    public BackgroundScroller() {
        background1 = new Background(0);
        background2 = new Background(background1.getEndX());
    }

    public void update(float delta) {
        background1.update(delta);
        background2.update(delta);

        if (background1.isNotOnScreen()) {
            background1.reset(background2.getEndX());
        } else if (background2.isNotOnScreen()) {
            background2.reset(background1.getEndX());
        }
    }

    public Background getBackground1() {
        return background1;
    }

    public Background getBackground2() {
        return background2;
    }

}
