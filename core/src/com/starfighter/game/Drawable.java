/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface Drawable {

    void draw(SpriteBatch spriteBatch);
}
