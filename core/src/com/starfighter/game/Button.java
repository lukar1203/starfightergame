/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Button implements Drawable {

    private Rectangle bounds;
    private TextureRegion textureRegion;
    private Vector2 position;
    private float width;
    private float height;

    public Button(float x, float y, TextureRegion textureRegion, int extender) {
        position = new Vector2(x, y);
        this.textureRegion = textureRegion;
        width = textureRegion.getRegionWidth();
        height = textureRegion.getRegionHeight();
        this.bounds = new Rectangle(x - extender / 2, y - extender / 2, width + extender,
                height + extender);
    }

    public void setTextureRegion(TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(textureRegion, position.x, position.y, width, height);
    }

    public boolean contains(float x, float y) {
        return bounds.contains(x, y);
    }

    public float getHeight() {
        return height;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }
}
