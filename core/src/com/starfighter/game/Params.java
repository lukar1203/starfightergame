/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game;

import com.badlogic.gdx.Gdx;

public class Params {

    public static final float MUSIC_VOLUME = 0.2F;
    public static final String HIGH_SCORES_PREF = "high_scores";
    public static final boolean DEBUG = false;

    //region SCREEN
    public static final float SCREEN_HEIGHT = 720;
    public static final float SCREEN_WIDTH = Gdx.graphics.getWidth() / (Gdx.graphics.getHeight() /
            SCREEN_HEIGHT);
    public static final float SCALE_FACTOR_X = SCREEN_WIDTH / Gdx.graphics.getWidth();
    public static final float SCALE_FACTOR_Y = SCREEN_HEIGHT / Gdx.graphics.getHeight();
    //endregion

    //region DIFFICULTY
    public static final int STARFIGHTER_MAX_HP = 3;
    public static final int BASIC_ENEMY_SPAWN_RATE = 50;
    public static final int ADVANCED_ENEMY_SPAWN_RATE = 50;
    public static final int BOSS_SPAWN_TIME = 30;
    public static final int MAX_ENEMIES = 30;
    public static final float STARFIGTER_FIRE_DELAY = 0.15f;
    public static final float BASIC_ENEMY_FIRE_DELAY = 1.5f;
    public static final float ADVANCED_ENEMY_FIRE_DELAY = 1.4f;
    public static final float BOSS_FIRE_DELAY = 1.1f;
    public static final float DIFFICULTY_UPDATE_DELAY = 1.0f;
    public static final float DIFFICULTY_COEFFICIENT_X = 1.01228f;
    public static final float DIFFICULTY_COEFFICIENT_Y = 0.5f;
    //endregion

    private Params() {
    }
}
