/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {

    public static TextureRegion background;
    public static TextureRegion starfighter;
    public static TextureRegion basicEnemy;
    public static TextureRegion advancedEnemy;
    public static TextureRegion boss;
    public static TextureRegion projectile;
    public static TextureRegion bossProjectile;
    public static ParticleEffect explosionEffect;
    public static TextureRegion mainScreen;
    public static TextureRegion sButton;
    public static TextureRegion pauseButton;
    public static TextureRegion unPauseButton;
    public static TextureRegion playAgainButton;
    public static TextureRegion highScoresButton;
    public static Sprite bar;
    public static BitmapFont font;
    public static Sound laser;
    public static Sound laser2;
    public static Sound explosion;
    public static Sound hit;
    public static Music music;
    private static TextureAtlas atlas;

    public static void load() {
        atlas = new TextureAtlas("data/textures.pack");

        background = atlas.findRegion("background");
        background.flip(false, true);

        starfighter = atlas.findRegion("starfighter");
        basicEnemy = atlas.findRegion("basicEnemy");
        advancedEnemy = atlas.findRegion("advancedEnemy");

        boss = atlas.findRegion("boss");

        projectile = atlas.findRegion("projectile");
        bossProjectile = atlas.findRegion("bossProjectile");
        mainScreen = atlas.findRegion("mainScreen");
        mainScreen.flip(false, true);

        explosionEffect = new ParticleEffect();
        explosionEffect.load(Gdx.files.internal("data/explosion.p"),
                Assets.atlas);

        playAgainButton = atlas.findRegion("playAgainButton");
        playAgainButton.flip(false, true);
        highScoresButton = atlas.findRegion("highScoresButton");
        highScoresButton.flip(false, true);
        sButton = atlas.findRegion("sButton");
        sButton.flip(false, true);
        pauseButton = atlas.findRegion("pauseButton");
        pauseButton.flip(false, true);
        unPauseButton = atlas.findRegion("unPauseButton");
        unPauseButton.flip(false, true);

        bar = new Sprite(atlas.findRegion("bar"));
        font = new BitmapFont(Gdx.files.internal("data/font.fnt"),
                atlas.findRegion("font"), true);
        laser = Gdx.audio.newSound(Gdx.files.internal("data/laser.wav"));
        laser2 = Gdx.audio.newSound(Gdx.files.internal("data/laser2.wav"));
        explosion = Gdx.audio
                .newSound(Gdx.files.internal("data/explosion.ogg"));
        hit = Gdx.audio.newSound(Gdx.files.internal("data/hit.wav"));
        music = Gdx.audio.newMusic(Gdx.files.internal("data/music.ogg"));
    }

    public static void dispose() {
        atlas.dispose();
        font.dispose();
        laser.dispose();
        laser2.dispose();
        explosion.dispose();
        explosionEffect.dispose();
        hit.dispose();
        music.dispose();
    }
}
