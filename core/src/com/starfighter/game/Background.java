/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Background implements Updatable, Drawable {

    private TextureRegion textureRegion;
    private Vector2 position;
    private Vector2 velocity;
    private float width;
    private float height;
    private boolean isOnScreen;

    public Background(float x) {
        position = new Vector2(x, (float) 0);
        velocity = new Vector2(BackgroundScroller.SPEED, 0);
        textureRegion = Assets.background;
        width = textureRegion.getRegionWidth();
        height = textureRegion.getRegionHeight();

        isOnScreen = true;
    }

    public void reset(float x) {
        position.x = x;
        isOnScreen = true;
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(textureRegion, position.x, position.y, width, height);
    }

    @Override
    public void update(float delta) {
        position.add(velocity.x * delta, velocity.y * delta);

        if (position.x + width < 0) {
            isOnScreen = false;
        }
    }

    public float getEndX() {
        return position.x + width;
    }

    public boolean isNotOnScreen() {
        return !isOnScreen;
    }
}
