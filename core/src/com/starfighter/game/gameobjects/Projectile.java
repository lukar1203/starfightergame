/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.starfighter.game.Status;

public class Projectile extends GameObject {

    public void init(float x, float y, TextureRegion projectile, float velocityX,
                     float velocityY, Status status) {
        super.init(x, y, projectile.getRegionWidth(), projectile.getRegionHeight(),
                projectile);

        alive = true;
        hp = 100;
        velocity.x = velocityX;
        velocity.y = velocityY;
        this.status = status;
    }

    public void update(float delta) {
        position.add(velocity.x * delta, velocity.y * delta);
        bounds.set(position.x, position.y, width, height);
    }
}
