/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.gameobjects.enemies;

import com.badlogic.gdx.math.MathUtils;
import com.starfighter.game.Assets;
import com.starfighter.game.GameWorld;

public class AdvancedEnemy extends Enemy {

    private int numberOfTicks;
    private int random;

    public void init(float x, float y, GameWorld gameWorld) {
        super.init(x, y, Assets.advancedEnemy.getRegionWidth(),
                Assets.advancedEnemy.getRegionHeight(), Assets.advancedEnemy, gameWorld);

        hp = 1;
        velocity.x = -200;
        value = 250;
        delay = 2f;
        random = MathUtils.random(100, 200);
    }

    @Override
    public void update(float delta) {
        timeSinceLastShot += delta;
        updatePosition(delta);
        updateCannons();

        super.update(delta);
    }

    @Override
    protected void updatePosition(float delta) {
        numberOfTicks++;

        position.y = (float) (random * Math.sin(numberOfTicks * 0.5 * Math.PI / 40))
                + gameWorld.getScreenHeight() / 2f - height / 2;
        position.x += velocity.x * delta;

        if (position.y < 0) {
            position.y = 0;
        }

        if (position.y > gameWorld.getScreenHeight() - height) {
            position.y = gameWorld.getScreenHeight() - height;
        }
    }

    @Override
    protected void updateCannons() {
        if (timeSinceLastShot >= delay) {
            timeSinceLastShot = 0;
            fire();
        }
    }

    private void fire() {
        fireCannon(height / 2 - 3, -600.0f, 0.0f);
        Assets.laser2.play();
    }

}
