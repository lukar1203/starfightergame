/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

/**
 *
 */
package com.starfighter.game.gameobjects.enemies;

import com.starfighter.game.GameWorld;
import com.starfighter.game.Status;
import com.starfighter.game.gameobjects.Ship;

public abstract class Enemy extends Ship {

    protected int value;

    public Enemy() {
        status = Status.ENEMY;
    }

    public void init(float x, float y, GameWorld gameWorld) {
        super.init(x, y, width, height, textureRegion, gameWorld);
        alive = true;
        status = Status.ENEMY;
    }

    public int getValue() {
        return value;
    }
}
