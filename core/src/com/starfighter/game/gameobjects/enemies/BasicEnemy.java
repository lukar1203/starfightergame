/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.gameobjects.enemies;

import com.starfighter.game.Assets;
import com.starfighter.game.GameWorld;

public class BasicEnemy extends Enemy {

    public void init(float x, float y, GameWorld gameWorld) {
        super.init(x, y, Assets.basicEnemy.getRegionWidth(), Assets.basicEnemy.getRegionHeight(),
                Assets.basicEnemy, gameWorld);

        hp = 1;
        velocity.x = -200;
        value = 100;
        delay = 2.0f;
    }

    @Override
    public void update(float delta) {
        timeSinceLastShot += delta;
        updatePosition(delta);
        updateCannons();

        super.update(delta);
    }

    @Override
    protected void updatePosition(float delta) {
        position.add(velocity.x * delta, velocity.y * delta);
    }

    @Override
    protected void updateCannons() {
        if (timeSinceLastShot >= delay) {
            timeSinceLastShot = 0;
            fire();
        }
    }

    private void fire() {
        fireCannon(height / 2 - 3, -600.0f, 0.0f);
        Assets.laser2.play();
    }
}
