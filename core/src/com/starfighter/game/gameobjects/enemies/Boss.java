/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.gameobjects.enemies;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.starfighter.game.Assets;
import com.starfighter.game.GameWorld;

public class Boss extends Enemy {

    private float secondCannonTimeSinceLastShot;
    private int numberOfTicks;
    private int random;

    public void init(float x, float y, GameWorld gameWorld) {
        super.init(x, y, Assets.boss.getRegionWidth(), Assets.boss.getRegionHeight(),
                Assets.boss, gameWorld);

        hp = 10;
        velocity.x = -200;
        value = 1000;
        delay = 2f;
        random = MathUtils.random(100, 200);
    }

    @Override
    public void update(float delta) {
        timeSinceLastShot += delta;
        secondCannonTimeSinceLastShot += delta;
        updatePosition(delta);
        updateCannons();

        super.update(delta);
    }

    @Override
    protected void updatePosition(float delta) {
        if (!(position.x < gameWorld.getScreenWidth() - 128)) {
            position.add(velocity.x * delta, velocity.y * delta);
        }

        numberOfTicks++;
        position.y = (float) (random * Math.sin(numberOfTicks * 0.5 * Math.PI / 40))
                + gameWorld.getScreenHeight() / 2f - height / 2;

        if (position.y < 0) {
            position.y = 0;
        }

        if (position.y > gameWorld.getScreenHeight() - height) {
            position.y = gameWorld.getScreenHeight() - height;
        }
    }

    @Override
    protected void updateCannons() {
        if (timeSinceLastShot >= delay / 0.9f) {
            timeSinceLastShot = 0;
            fire(1);
        }
        if (secondCannonTimeSinceLastShot >= delay) {
            secondCannonTimeSinceLastShot = 0;
            fire(2);
        }
    }

    private void fire(int cannonNumber) {
        Vector2 target = target(new Vector2(600, 0));
        if (cannonNumber == 1) {
            fireCannon(24, target.x, target.y, Assets.bossProjectile);
            Assets.laser2.play();
        } else {
            fireCannon(86, target.x, target.y, Assets.bossProjectile);
            Assets.laser2.play();
        }
    }

    private Vector2 target(Vector2 projectileVelocity) {
        float angle = gameWorld.getStarfighter().getPosition().cpy().sub(position).angle();
        return projectileVelocity.rotate(angle);
    }
}
