/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.starfighter.game.Assets;
import com.starfighter.game.Drawable;
import com.starfighter.game.Params;
import com.starfighter.game.Status;
import com.starfighter.game.Updatable;

public abstract class GameObject implements Updatable, Drawable {

    protected Vector2 position;
    protected Vector2 velocity;
    protected Rectangle bounds;
    protected int width;
    protected int height;
    protected TextureRegion textureRegion;
    protected Status status;
    protected int hp;
    protected int value;
    protected boolean alive;

    public GameObject() {
        position = new Vector2();
        velocity = new Vector2();
        bounds = new Rectangle();
    }

    public void init(float x, float y, int width, int height, TextureRegion textureRegion) {
        position.set(x, y);
        this.width = width;
        this.height = height;
        bounds.set(x, y, width, height);
        this.textureRegion = textureRegion;
        alive = true;
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(textureRegion, position.x, position.y, width, height);
    }

    public boolean hit(Rectangle target) {
        if (Intersector.overlaps(bounds, target)) {
            Assets.hit.play();

            return true;
        }

        return false;
    }

    public void explode(ParticleEffectPool explosionEffectPool,
                        Array<ParticleEffectPool.PooledEffect> explosionEffects) {
        setDead();
        ParticleEffectPool.PooledEffect effect = explosionEffectPool.obtain();
        effect.setPosition(getX() + getHeight()
                / 2, getY() + getWidth() / 2);
        effect.start();
        explosionEffects.add(effect);
        Assets.explosion.play();
    }

    public void setDead() {
        alive = false;
    }

    public float getX() {
        return position.x;
    }

    public int getHeight() {
        return height;
    }

    public float getY() {
        return position.y;
    }

    public int getWidth() {
        return width;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public boolean isOnscreen() {
        return position.x <= Params.SCREEN_WIDTH + 100 && position.x >= -100;
    }

    public Vector2 getPosition() {
        return position;
    }

    public Status getStatus() {
        return status;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getValue() {
        return value;
    }

    public boolean isAlive() {
        return alive;
    }
}
