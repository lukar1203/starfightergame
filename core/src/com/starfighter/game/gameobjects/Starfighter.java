/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game.gameobjects;

import com.starfighter.game.Assets;
import com.starfighter.game.GameWorld;
import com.starfighter.game.Params;
import com.starfighter.game.Status;

public class Starfighter extends Ship {

    public static final float VELOCITY_DIVISOR = 1.25f;
    public static final float PROJECTILE_VELOCITY_X = 600.0f;
    public static final float PROJECTILE_VELOCITY_Y = 0.0f;
    private boolean movingUp;
    private boolean movingDown;
    private boolean shooting;

    public void init(float y, GameWorld gameWorld) {
        super.init((float) GameWorld.STAR_FIGHTER_LOCATION_X, y, Assets.starfighter.getRegionWidth(), Assets.starfighter.getRegionHeight(),
                Assets.starfighter, gameWorld);

        velocity.y = gameWorld.getScreenHeight() / VELOCITY_DIVISOR;
        hp = Params.STARFIGHTER_MAX_HP;
        status = Status.ALLY;
        delay = Params.STARFIGTER_FIRE_DELAY;
    }

    public void update(float delta) {
        timeSinceLastShot += delta;
        updatePosition(delta);
        updateCannons();

        super.update(delta);
    }

    @Override
    protected void updatePosition(float delta) {
        if (movingUp) {
            position.y -= velocity.y * delta;
        }

        if (movingDown) {
            position.y += velocity.y * delta;
        }

        if (position.y < 0) {
            position.y = 0;
        }

        if (position.y > gameWorld.getScreenHeight() - height) {
            position.y = gameWorld.getScreenHeight() - height;
        }
    }

    @Override
    protected void updateCannons() {
        if (shooting && timeSinceLastShot >= delay) {
            timeSinceLastShot = 0;
            fire();
        }
    }

    public void fire() {
        fireCannon(height / 2 - Assets.projectile.getRegionHeight() / 2, PROJECTILE_VELOCITY_X,
                PROJECTILE_VELOCITY_Y);
        Assets.laser.play();
    }

    public void moveUp(boolean bool) {
        if (movingDown && bool) {
            movingDown = false;
        }
        movingUp = bool;
    }

    public void moveDown(boolean bool) {
        if (movingUp && bool) {
            movingUp = false;
        }
        movingDown = bool;
    }

    public void setShooting(boolean shooting) {
        this.shooting = shooting;
    }
}
