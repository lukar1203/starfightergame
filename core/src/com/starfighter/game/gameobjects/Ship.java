/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

/**
 *
 */
package com.starfighter.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pools;
import com.starfighter.game.Assets;
import com.starfighter.game.GameWorld;
import com.starfighter.game.Status;

public abstract class Ship extends GameObject {

    protected GameWorld gameWorld;
    protected float delay;
    protected float timeSinceLastShot;

    public void init(float x, float y, int width, int height,
                     TextureRegion textureRegion, GameWorld gameWorld) {
        super.init(x, y, width, height, textureRegion);

        this.gameWorld = gameWorld;
    }

    protected void fireCannon(int height, float velocityX, float velocityY) {
        fireCannon(height, velocityX, velocityY, Assets.projectile);
    }

    protected void fireCannon(int height, float velocityX, float velocityY,
                              TextureRegion projectileTexture) {

        float startX;

        if (status == Status.ENEMY) {
            startX = getX();
        } else {
            startX = getX() + width * 0.7f;
        }

        float startY = getY() + height;

        Projectile projectile = Pools.obtain(Projectile.class);
        projectile.init(startX, startY, projectileTexture, velocityX, velocityY, status);

        gameWorld.getGameObjects().add(projectile);
    }

    public void update(float delta) {
        this.bounds.set(position.x, position.y, width, height);
    }

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public void setDelay(float delay) {
        this.delay = delay;
    }

    protected abstract void updatePosition(float delta);

    protected abstract void updateCannons();
}
