/*
 * Copyright (c) 2014. Łukasz Karpiński <lukar1203@gmail.com>
 */

package com.starfighter.game;

import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;
import com.starfighter.game.gameobjects.GameObject;
import com.starfighter.game.gameobjects.Starfighter;
import com.starfighter.game.helpers.CollisionManager;
import com.starfighter.game.helpers.EnemyManager;
import com.starfighter.game.helpers.ScoreManager;
import com.starfighter.game.screens.ScoreScreen;

public class GameWorld {

    public static final int STAR_FIGHTER_LOCATION_X = 80;
    private EnemyManager enemyManager;
    private ScoreManager scoreManager;
    private CollisionManager collisionManager;
    private StarfighterGame game;
    private int score;
    private float screenHeight;
    private float screenWidth;
    private Starfighter starfighter;
    private BackgroundScroller backgroundScroller;
    private Button shoot;
    private Button pause;
    private Array<GameObject> gameObjects;
    private Array<PooledEffect> explosionEffects;
    private ParticleEffectPool explosionEffectPool;
    private String name;
    private Array<GameObject> deletedGameObjects;

    public GameWorld(StarfighterGame game, BackgroundScroller backgroundScroller) {
        this.game = game;
        this.backgroundScroller = backgroundScroller;

        screenHeight = Params.SCREEN_HEIGHT;
        screenWidth = Params.SCREEN_WIDTH;

        starfighter = new Starfighter();
        gameObjects = new Array<GameObject>(false, 150);
        deletedGameObjects = new Array<GameObject>(false, 50);
        explosionEffectPool = new ParticleEffectPool(Assets.explosionEffect, 10, 20);
        explosionEffects = new Array<ParticleEffectPool.PooledEffect>();

        enemyManager = new EnemyManager(this);
        collisionManager = new CollisionManager(this);
        scoreManager = new ScoreManager();

        starfighter.init(Params.SCREEN_HEIGHT / 2 - 32, this);
        gameObjects.add(starfighter);

        shoot = new Button(Params.SCREEN_WIDTH - 120, Params.SCREEN_HEIGHT - 120, Assets.sButton, 180);
        pause = new Button(Params.SCREEN_WIDTH - Assets.pauseButton.getRegionWidth() - 10, 12,
                Assets.pauseButton, 30);

    }

    public Array<GameObject> getGameObjects() {
        return gameObjects;
    }

    public void update(float delta) {
        if (game.getGameState() == StarfighterGame.State.RUNNING) {
            pause.setTextureRegion(Assets.pauseButton);

            if (name != null) {
                showScoreScreen();
            }

            backgroundScroller.update(delta);
            updateExplosions(delta);
            enemyManager.spawnEnemies(delta);
            collisionManager.handleCollisions();
            updateGameObjects(delta);
        } else {
            pause.setTextureRegion(Assets.unPauseButton);
        }
    }

    private void showScoreScreen() {
        scoreManager.writeScore(name, score);
        game.setScreen(new ScoreScreen(game, scoreManager));
    }

    private void updateExplosions(float delta) {
        for (PooledEffect effect : explosionEffects) {
            effect.update(delta);
            if (effect.isComplete()) {
                explosionEffects.removeValue(effect, false);
                effect.free();
            }
        }
    }

    private void updateGameObjects(float delta) {
        for (GameObject object : gameObjects) {
            if (object.isAlive()) {

                if (!object.isOnscreen()) {
                    object.setDead();
                } else {
                    object.update(delta);
                }
            }
        }

        for (GameObject object : gameObjects) {
            if (!object.isAlive()) {
                deletedGameObjects.add(object);
            }
        }

        for (GameObject object : deletedGameObjects) {
            if (object.getStatus() == Status.ENEMY && object.getHp() < 100) {
                enemyManager.decrementEnemiesCount();
            }

            gameObjects.removeValue(object, true);
            Pools.free(object);
        }

        deletedGameObjects.clear();
    }

    public Starfighter getStarfighter() {
        return starfighter;
    }

    public BackgroundScroller getBackgroundScroller() {
        return backgroundScroller;
    }

    public Button getShoot() {
        return shoot;
    }

    public float getScreenHeight() {
        return screenHeight;
    }

    public float getScreenWidth() {
        return screenWidth;
    }

    public Array<PooledEffect> getExplosionEffects() {
        return explosionEffects;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Button getPause() {
        return pause;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ParticleEffectPool getExplosionEffectPool() {
        return explosionEffectPool;
    }

    public StarfighterGame getGame() {
        return game;
    }
}
